package com.example.khale.navigationdrawerexample.services;

import com.example.khale.navigationdrawerexample.model.Weather;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface OpenWeatherAPI {

    String baseUrl = "http://api.openweathermap.org";

    @GET("/data/2.5/weather")
    Call<Weather> getWeather(@Query("id") int cityID, @Query("APPID") String appID);

    @GET("/data/2.5/weather")
    Call<Weather> getWeatherByCityName(@Query("q") String city, @Query("APPID") String appID);
}
