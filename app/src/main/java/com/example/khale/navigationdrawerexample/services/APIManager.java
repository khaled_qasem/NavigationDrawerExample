package com.example.khale.navigationdrawerexample.services;

import android.util.Log;

import com.example.khale.navigationdrawerexample.model.Weather;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class APIManager implements Callback<Weather> {
    private static final String TAG = APIManager.class.getName();

    public void start() {
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();

        String BASE_URL = "http://api.openweathermap.org";

        OkHttpClient mIntercepter = new OkHttpClient.Builder()
//                .cookieJar(mCookieJar)
//                .addInterceptor(new RequestResponseInterseptor(context))
                .addInterceptor(new Interceptor() {
                    @Override
                    public Response intercept(Chain chain) throws IOException {
                        Request request = chain.request().newBuilder().addHeader("Authorization", "Bearer " + "1ed6b7c1839e02bbf7a1b4a8dbca84d23127c68e").build();
                        return chain.proceed(request);
                    }}).build();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(mIntercepter)
                .build();

        OpenWeatherAPI openWeatherAPI = retrofit.create(OpenWeatherAPI.class);

        Call<Weather> call = openWeatherAPI.getWeather(2172797, "1143eef90b041bd2f9d6fe834538af60");
        call.enqueue(this);

    }


    @Override
    public void onResponse(Call<Weather> call, retrofit2.Response<Weather> response) {
        if(response.isSuccessful()) {
            Weather weather = response.body();
            Log.d(TAG, "onResponse: success"
                    +"lat=" + weather.getCoord().getLat()
                    +"lon="+weather.getCoord().getLon()
            );
        } else {
            Log.d("onResponse: erorrBody :", "Code: " + response.code() + " Message: " + response.message());
            Log.d(TAG, "onResponse: erorrBody : "+response.errorBody() );
        }
    }

    @Override
    public void onFailure(Call<Weather> call, Throwable t) {
        t.printStackTrace();
    }
}
